from django.db import models
from colorful.fields import RGBColorField
# Create your models here.

class Termine(models.Model):
    fach = models.CharField(default=True, max_length= 40)
    was = models.CharField(default=True, max_length= 40)
    datum = models.CharField(max_length=40)
    tage = models.TextField()
    farbe = RGBColorField(colors=['#15d604', '#fcad02', '#f7021f'])
    erledigt = models.BooleanField(default=False)

def __str__(self):
        erledigt_string = " "
        if self.erledigt:
            erledigt_string = "X"
        return "[{}] {} {} {} {} {}".format(erledigt_string, self.fach, self.was, self.datum, self.tage, self.farbe)