from django.shortcuts import render, redirect, get_object_or_404
from . import models

# Create your views here.
def index(request):
    context = {}
    context['einfuegen'] = models.Termine.objects.filter(erledigt=False)
    context['erledigt'] = models.Termine.objects.filter(erledigt=True)
    return render(request, 'pk/index.html', context)

def add(request):
    if 'add' in request.POST:
        item = models.Termine()
        item.fach = request.POST['fach']
        item.datum = request.POST['datum']
        item.tage = request.POST['tage']
        item.farbe = request.POST['farbe']
        item.was = request.POST['was']
        item.save()
    return redirect('index')

def delete(request):
    if 'id' in request.GET:
        item = get_object_or_404(models.Termine, id=request.GET.get('id'))
        item.delete()
    return redirect('index')

def refresh(request):
    if 'refresh' in request.POST:
        einfuegen = models.Contacts.objects.filter(erledigt=False)
        erledigt = models.Contacts.objects.filter(erledigt=True)
        print("Neu erledigt: {}".format(request.POST.getlist('neu_erledigt')))
        print("Bleiben erledigt: {}".format(request.POST.getlist('bleiben_erledigt')))
        gerade_erledigt = []
        for item in einfuegen:
            print(str(item.id))
            if str(item.id) in request.POST.getlist('neu_erledigt'):
                print('Item erledigt: {}'.format(item.id))
                item.erledigt=True
                gerade_erledigt.append(str(item.id))
                item.save()
        for item in erledigt:
            if str(item.id) not in request.POST.getlist('bleiben_erledigt') and str(item.id) not in gerade_erledigt:
                print('Item nicht mehr erledigt: {}'.format(item.id))
                item.erledigt=False
                item.save()
        return redirect('index')